![NeuroAnalyzer.jl](neuroanalyzer.png)

# NeuroAnalyzer-misc

This repository contains various NeuroAnalyzer-related scripts and utilities:
- `na_create_release.sh` generates zip archive of the stable release
- `na_generate_sysimage.sh` generates sysimage for faster NeuroAnalyzer startup
- `na_create_docker.sh` generates and pushes Docker image

## How to use the scripts

To generate the zipped release, run:
```shell
na_create_release.sh 0.22.10
```

To generate sysimage, run:
```shell
na_generate_sysimage.sh
```

To use the generated sysimage, run:
```shell
julia -JNeuroAnalyzer.so
```

## License

This software is licensed under [The 2-Clause BSD License](LICENSE).
