using LocalRegistry

reg_path = expanduser(ARGS[1])
@assert isdir(reg_path) "The JuliaRegistry folder does not exist or was not provided."
@info "Creating LocalRegistry"
create_registry("LocalRegistry", reg_path, description = "LocalRegistry")
@info "Done"

exit()