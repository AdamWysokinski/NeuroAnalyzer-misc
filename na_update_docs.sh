#! /bin/sh

cd /tmp
rm -rf update_docs 
mkdir update_docs
cd update_docs

git clone https://codeberg.org/AdamWysokinski/NeuroAnalyzer-docs NA-docs
git clone https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl

cd NeuroAnalyzer.jl/docs
rm -rf build
rm -f src/index.md
cat header.md > src/index.md
./template.sh >> src/index.md
julia make_md.jl
cp build/index.md ../../NA-docs/Documentation.md
rm -rf build
julia make_html.jl
mv build docs
sed -i 's/Edit on GitHub/Edit on Codeberg/g' docs/index.html
sed -i 's///g' docs/index.html
ncftpput -R -u $NA_FTP_USER -p $NA_FTP_PASS ftp.h7530498.hostingrd.pl / docs

cd ../../NA-docs
git config --global user.email $GIT_EMAIL
git config --global user.name "CI Builder"
git remote set-url origin https://$CODEBERG_TOKEN@codeberg.org/AdamWysokinski/NeuroAnalyzer-docs.git
git add --all
./push_docs.sh
git config --global user.name $GIT_NAME
