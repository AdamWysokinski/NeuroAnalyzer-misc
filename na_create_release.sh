#! /bin/sh

VERSION=$1

cd NeuroAnalyzer.jl
git archive --format zip --output ../NeuroAnalyzer-$VERSION.zip main
cd ..
