#! /usr/bin/zsh

cd ~/Documents/Code
rm -rf ~/Documents/Code/JuliaRegistry
gh repo delete https://github.com/AdamWysokinski/General --yes
gh repo fork https://github.com/JuliaRegistries/General --clone=false
git clone --depth 1 https://github.com/AdamWysokinski/General JuliaRegistry

cd JuliaRegistry
git checkout -b my-update
julia ~/Documents/Code/na_create_localregistry.jl "~/Documents/Code/JuliaRegistry"

cd ~/Documents/Code/NeuroAnalyzer.jl
git checkout main

cd ~/Documents/Code/NeuroStats.jl
git checkout main

# only necessary if there are new packages added to dependencies
# julia ~/Documents/Code/na_update_compat.jl
# julia ~/Documents/Code/na_compat_helper.jl

julia ~/Documents/Code/na_update_registry.jl "~/Documents/Code/JuliaRegistry"

cd ~/Documents/Code/JuliaRegistry
git push https://$GITHUB_TOKEN@github.com/AdamWysokinski/General.git

rm -rf ~/.julia/registries/LocalRegistry > /dev/null
rm -rf ~/Documents/Code/JuliaRegistry > /dev/null
gh repo delete https://github.com/AdamWysokinski/General --yes
