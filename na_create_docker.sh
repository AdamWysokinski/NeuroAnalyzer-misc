#! /bin/sh

cd NeuroAnalyzer.jl
docker build -t neuroanalyzer .
docker login
docker tag neuroanalyzer:latest neuroanalyzer/neuroanalyzer:latest
docker push neuroanalyzer/neuroanalyzer:latest
cd ..