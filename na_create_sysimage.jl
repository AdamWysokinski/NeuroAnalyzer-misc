using PackageCompiler
using Pkg

@info "Activating packages.."
Pkg.activate(".")
Pkg.add(url="https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl#master")
Pkg.instantiate()
@info "Generating sysimage.."
create_sysimage(["NeuroAnalyzer"], sysimage_path="NeuroAnalyzer.so")
@info "Done."

exit()